$(document).ready(function(){
    $('.statistics').DataTable({
        pageLength: 10,
        processing: true,
        order: [
            0, 'desc'
        ],
        dom: '<"html5buttons"B>lTfgitp',
        ajax: {
            url: 'statistics/getAjaxData',
            type: 'POST'
        },
        columns: [
            {
                data: 'date',
                title: 'Дата'
            },
            {
                data: 'url',
                title: 'URL'
            },
            {
                data: 'ip',
                title: 'IP'
            },
            {
                data: 'phone',
                title: 'Телефон'
            },
            {
                data: 'email',
                title: 'Email'
            },
        ],
        buttons: [
            {
                extend: 'copy'
            },
            {
                extend: 'csv',
                title: 'Статистика'
            },
            {
                extend: 'excel',
                title: 'Статистика'
            },
            {
                extend: 'pdf',
                title: 'Статистика'
            },
            {
                extend: 'print',
                title: 'Статистика',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]
    });
});