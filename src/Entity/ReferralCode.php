<?php

namespace App\Entity;

use App\EntityTrait\BaseTrait;

/**
 * Referral code
 */
class ReferralCode
{
    use BaseTrait;

    public function __construct()
    {
        $this->code = uniqid();
        $this->visitCount = 0;
        $this->registrationCount = 0;
    }

    /**
     * @var string Code
     */
    private $code;

    /**
     * @var int Visit count
     */
    private $visitCount;

    /**
     * @var int Registration count
     */
    private $registrationCount;

    /**
     * Sets code
     *
     * @param string $code Code
     *
     * @return ReferralCode This object
     */
    public function setCode(string $code): ReferralCode
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Gets code
     *
     * @return string Code
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * Sets visit count
     *
     * @param int $visitCount Visit count
     *
     * @return ReferralCode This object
     */
    public function setVisitCount(int $visitCount): ReferralCode
    {
        $this->visitCount = $visitCount;

        return $this;
    }

    /**
     * Gets visit count
     *
     * @return int Visit count
     */
    public function getVisitCount(): int
    {
        return $this->visitCount;
    }

    /**
     * Sets registration count
     *
     * @param int $registrationCount Registration count
     *
     * @return ReferralCode This object
     */

    public function setRegistrationCount(int $registrationCount): ReferralCode
    {
        $this->registrationCount = $registrationCount;

        return $this;
    }

    /**
     * Gets registration count
     *
     * @return int Registration count
     */
    public function getRegistrationCount(): int
    {
        return $this->registrationCount;
    }
}