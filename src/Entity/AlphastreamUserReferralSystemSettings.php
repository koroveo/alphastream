<?php

namespace App\Entity;

use App\EntityTrait\BaseTrait;

/**
 * Alphastream user referral system settings
 */
class AlphastreamUserReferralSystemSettings
{
    use BaseTrait;

    /**
     * @var int Alphastream user ID
     */
    private $alphastreamUserId;

    /**
     * @var string Referral system settings ID
     */
    private $referralSystemSettingsId;

    /**
     * Sets level
     *
     * @param int $level Level
     *
     * @return ReferralSystemSettings This object
     */
    public function setLevel(int $level): ReferralSystemSettings
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Gets level
     *
     * @return int Level
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * Sets percent
     *
     * @param string $percent Percent
     *
     * @return ReferralSystemSettings This object
     */
    public function setPercent(string $percent): ReferralSystemSettings
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Gets percent
     *
     * @return string Percent
     */
    public function getPercent(): string
    {
        return $this->percent;
    }
}