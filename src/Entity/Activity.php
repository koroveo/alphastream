<?php

namespace App\Entity;

use App\EntityTrait\BaseTrait;

/**
 * Activity
 */
class Activity
{
    use BaseTrait;

    /**
     * @var int Counter id
     */
    private $counterId;

    /**
     * @var string Url
     */
    private $url;

    /**
     * @var string Ip
     */
    private $ip;

    /**
     * @var string Cookie value
     */
    private $cookieValue;

    /**
     * @var string Phone
     */
    private $phone;

    /**
     * @var string Email
     */
    private $email;

    /**
     * @var string Phone 2
     */
    private $phone2;

    /**
     * @var string Phone 3
     */
    private $phone3;

    /**
     * @var string Email 2
     */
    private $email2;

    /**
     * @var string Email 3
     */
    private $email3;

    /**
     * @var Counter Counter
     */
    private $counter;

    /**
     * Sets counter id
     *
     * @param int $counterId Counter id
     *
     * @return Activity This object
     */
    public function setCounterId(int $counterId): Activity
    {
        $this->counterId = $counterId;

        return $this;
    }

    /**
     * Gets counter id
     *
     * @return int Counter
     */
    public function getCounterId(): int
    {
        return $this->counterId;
    }

    /**
     * Sets url
     *
     * @param string $url Url
     *
     * @return Activity This object
     */
    public function setUrl(string $url): Activity
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Gets url
     *
     * @return string|null Url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Sets ip
     *
     * @param string $ip Ip
     *
     * @return Activity This object
     */
    public function setIp(string $ip): Activity
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Gets ip
     *
     * @return string|null Ip
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Sets cookie value
     *
     * @param string $cookieValue Cookie value
     *
     * @return Activity This object
     */
    public function setCookieValue(string $cookieValue): Activity
    {
        $this->cookieValue = $cookieValue;

        return $this;
    }

    /**
     * Gets cookie value
     *
     * @return string|null Cookie value
     */
    public function getCookieValue()
    {
        return $this->cookieValue;
    }

    /**
     * Sets counter
     *
     * @param Counter $counter Counter
     *
     * @return Activity This object
     */
    public function setCounter(Counter $counter = null): Activity
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * Gets counter
     *
     * @return Counter Counter
     */
    public function getCounter(): Counter
    {
        return $this->counter;
    }

    /**
     * Sets phone
     *
     * @param string $phone Phone
     *
     * @return Activity This object
     */
    public function setPhone(string $phone): Activity
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Gets phone
     *
     * @return string|null Phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets email
     *
     * @param string $email Email
     *
     * @return Activity This object
     */
    public function setEmail(string $email): Activity
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Gets email
     *
     * @return string|null Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets phone 2
     *
     * @param string $phone2 Phone 2
     *
     * @return Activity This object
     */
    public function setPhone2(string $phone2): Activity
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Gets phone 2
     *
     * @return string|null Phone 2
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Sets phone 3
     *
     * @param string $phone3 Phone 3
     *
     * @return Activity This object
     */
    public function setPhone3(string $phone3): Activity
    {
        $this->phone3 = $phone3;

        return $this;
    }

    /**
     * Gets phone 3
     *
     * @return string|null Phone 3
     */
    public function getPhone3()
    {
        return $this->phone3;
    }

    /**
     * Sets email 2
     *
     * @param string $email2 Email 2
     *
     * @return Activity This object
     */
    public function setEmail2(string $email2): Activity
    {
        $this->email2 = $email2;

        return $this;
    }

    /**
     * Gets email 2
     *
     * @return string|null Email 2
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Sets email 3
     *
     * @param string $email3 Email 3
     *
     * @return Activity This object
     */
    public function setEmail3(string $email3): Activity
    {
        $this->email3 = $email3;

        return $this;
    }

    /**
     * Gets email 3
     *
     * @return string|null Email 3
     */
    public function getEmail3()
    {
        return $this->email3;
    }
}