<?php

namespace App\Entity;

use App\EntityTrait\BaseTrait;

/**
 * Data from dbpool
 */
class DbPoolData
{
    use BaseTrait;

    /**
     * @var string Caltat session id
     */
    private $caltat_s_id;

    /**
     * @var string Partner session id
     */
    private $partner_s_id;

    /**
     * @var string Email
     */
    private $email;

    /**
     * @var string Phone
     */
    private $phone;

    /**
     * @var string Partner Id
     */
    private $partnerId;

    /**
     * Sets caltat session id
     *
     * @param string $caltat_s_id Caltat session id
     *
     * @return DbPoolData This object
     */
    public function setCaltatSId(string $caltat_s_id): DbPoolData
    {
        $this->caltat_s_id = $caltat_s_id;

        return $this;
    }

    /**
     * Gets caltat session id
     *
     * @return string Caltat session id
     */
    public function getCaltatSId(): string
    {
        return $this->caltat_s_id;
    }

    /**
     * Sets partner session id
     *
     * @param string $partner_s_id Partner session id
     *
     * @return DbPoolData This object
     */
    public function setPartnerSId(string $partner_s_id): DbPoolData
    {
        $this->partner_s_id = $partner_s_id;

        return $this;
    }

    /**
     * Gets partner session id
     *
     * @return string Partner session id
     */
    public function getPartnerSId(): string
    {
        return $this->partner_s_id;
    }

    /**
     * Sets email
     *
     * @param string $email Email
     *
     * @return DbPoolData This object
     */
    public function setEmail(string $email): DbPoolData
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Gets email
     *
     * @return string Email
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Sets phone
     *
     * @param string $phone Phone
     *
     * @return DbPoolData This object
     */
    public function setPhone(string $phone): DbPoolData
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Gets phone
     *
     * @return string Phone
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * Sets partner Id
     *
     * @param string $partnerId Partner id
     *
     * @return DbPoolData This object
     */
    public function setPartnerId(string $partnerId): DbPoolData
    {
        $this->partnerId = $partnerId;

        return $this;
    }

    /**
     * Gets partner Id
     *
     * @return string Partner id
     */
    public function getPartnerId(): string
    {
        return $this->partnerId;
    }
}