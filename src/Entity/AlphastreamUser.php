<?php

namespace App\Entity;

use App\EntityTrait\BaseTrait;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Alphastream user
 */
class AlphastreamUser implements UserInterface
{
    use BaseTrait;

    public function __construct()
    {
        $this->roles = ['ROLE_USER'];
    }

    /**
     * @var string Email
     */
    private $email;

    /**
     * @var string Password
     */
    private $password;

    /**
     * @var string Plain password
     */
    private $plainPassword;

    /**
     * @var string Password request token
     */
    private $passwordRequestToken;

    /**
     * @var array Roles
     */
    private $roles;

    /**
     * @var int Owned referral code ID
     */
    private $ownedReferralCodeId;

    /**
     * @var int Invited by referral code ID
     */
    private $invitedByReferralCodeId;

    /**
     * Sets email
     *
     * @param string $email Email
     *
     * @return AlphastreamUser This object
     */
    public function setEmail(string $email): AlphastreamUser
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Gets email
     *
     * @return string Email
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Sets password
     *
     * @param string $password Password
     *
     * @return AlphastreamUser This object
     */
    public function setPassword(string $password): AlphastreamUser
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Gets password
     *
     * @return string Password
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Sets plain password
     *
     * @param string $plainPassword Plain password
     *
     * @return AlphastreamUser This object
     */
    public function setPlainPassword(string $plainPassword): AlphastreamUser
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * Gets plain Password
     *
     * @return string PlainPassword
     */
    public function getPlainPassword(): string
    {
        return $this->plainPassword;
    }

    /**
     * Sets password request token
     *
     * @param string $passwordRequestToken Password request token
     *
     * @return AlphastreamUser This object
     */
    public function setPasswordRequestToken(string $passwordRequestToken): AlphastreamUser
    {
        $this->passwordRequestToken = $passwordRequestToken;

        return $this;
    }

    /**
     * Gets password request token
     *
     * @return string PasswordRequestToken
     */
    public function getPasswordRequestToken(): string
    {
        return $this->passwordRequestToken;
    }

    /**
     * Sets roles
     *
     * @param array $roles Roles
     *
     * @return AlphastreamUser This object
     */
    public function setRoles(array $roles): AlphastreamUser
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Gets roles
     *
     * @return array Roles
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    public function eraseCredentials(): AlphastreamUser
    {
        $this->plainPassword = null;

        return $this;
    }

    /**
     * Sets owned referral code ID
     *
     * @param int $ownedReferralCodeId Owned referral code ID
     *
     * @return AlphastreamUser This object
     */
    public function setOwnedReferralCodeId(int $ownedReferralCodeId): AlphastreamUser
    {
        $this->ownedReferralCodeId = $ownedReferralCodeId;

        return $this;
    }

    /**
     * Gets owned referral code ID
     *
     * @return int ReferralCode Owned referral code ID
     */
    public function getOwnedReferralCodeId(): int
    {
        return $this->ownedReferralCodeId;
    }

    /**
     * Sets invited by referral code ID
     *
     * @param int $invitedByReferralCodeId Invited by referral code ID
     *
     * @return AlphastreamUser This object
     */
    public function setInvitedByReferralCodeID(int $invitedByReferralCodeId): AlphastreamUser
    {
        $this->invitedByReferralCodeId = $invitedByReferralCodeId;

        return $this;
    }

    /**
     * Gets invited by referral code ID
     *
     * @return int ReferralCode Invited by referral code ID
     */
    public function getInvitedByReferralCodeID(): int
    {
        return $this->invitedByReferralCodeId;
    }
}