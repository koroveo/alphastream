<?php

namespace App\Entity;

use App\EntityTrait\BaseTrait;

/**
 * Counter
 */
class Counter
{
    use BaseTrait;

    /**
     * @var string Counter
     */
    private $code;

    /**
     * Sets code
     *
     * @param string $code Counter
     *
     * @return Counter This object
     */
    public function setCode(string $code): Counter
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Gets code
     *
     * @return string Counter
     */
    public function getCode(): string
    {
        return $this->code;
    }
}