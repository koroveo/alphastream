<?php

namespace App\Entity;

use App\EntityTrait\BaseTrait;

/**
 * Referral system settings
 */
class ReferralSystemSettings
{
    use BaseTrait;

    /**
     * @var int Level
     */
    private $level;

    /**
     * @var string Percent
     */
    private $percent;

    /**
     * Sets level
     *
     * @param int $level Level
     *
     * @return ReferralSystemSettings This object
     */
    public function setLevel(int $level): ReferralSystemSettings
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Gets level
     *
     * @return int Level
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * Sets percent
     *
     * @param string $percent Percent
     *
     * @return ReferralSystemSettings This object
     */
    public function setPercent(string $percent): ReferralSystemSettings
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Gets percent
     *
     * @return string Percent
     */
    public function getPercent(): string
    {
        return $this->percent;
    }
}