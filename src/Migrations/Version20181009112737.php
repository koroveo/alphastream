<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181009112737 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE activity ADD phone_2 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE activity ADD phone_3 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE activity ADD email_2 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE activity ADD email_3 VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE activity DROP phone_2');
        $this->addSql('ALTER TABLE activity DROP phone_3');
        $this->addSql('ALTER TABLE activity DROP email_2');
        $this->addSql('ALTER TABLE activity DROP email_3');
    }
}
