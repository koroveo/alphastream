<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181129144424 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE alphastream_user ADD referral_code_id INT NOT NULL');
        $this->addSql('ALTER TABLE alphastream_user ADD CONSTRAINT FK_20327F537EFAA231 FOREIGN KEY (referral_code_id) REFERENCES referral_code (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_20327F537EFAA231 ON alphastream_user (referral_code_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE alphastream_user DROP CONSTRAINT FK_20327F537EFAA231');
        $this->addSql('DROP INDEX UNIQ_20327F537EFAA231');
        $this->addSql('ALTER TABLE alphastream_user DROP referral_code_id');
    }
}
