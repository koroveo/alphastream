<?php

namespace App\Controller;

use App\Entity\AlphastreamUser;
use App\Form\NewPasswordType;
use App\Form\PasswordRequestType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResettingController extends AbstractController
{
    public function resetPassword(Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(PasswordRequestType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $token = bin2hex(random_bytes(32));
            $user = $entityManager->getRepository(AlphastreamUser::class)->findOneBy(['email' => $email]);

            if ($user instanceof AlphastreamUser) {
                $user->setPasswordRequestToken($token);
                $entityManager->flush();

                // TODO: send email with SwiftMailer or anything else here
                $this->addFlash('success', "An email has been sent to your address");

                return $this->redirectToRoute('resetPassword');
            }
        }

        return $this->render('index/reset-password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function resetPasswordCheck(Request $request, string $token, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder, TokenStorageInterface $tokenStorage, SessionInterface $session)
    {
        $user = $entityManager->getRepository(AlphastreamUser::class)->findOneBy(['passwordRequestToken' => $token]);

        if (!$token || !$user instanceof AlphastreamUser) {
            $this->addFlash('danger', "User not found");

            return $this->redirectToRoute('resetPassword');
        }

        $form = $this->createForm(NewPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $plainPassword = $form->get('password')->getData();
            $password = $encoder->encodePassword($user, $plainPassword);

            $user->setPassword($password);
            $user->setPasswordRequestToken(null);

            $entityManager->flush();

            $token = new UsernamePasswordToken($user, $password, 'main');
            $tokenStorage->setToken($token);
            $session->set('_security_main', serialize($token));

            $this->addFlash('success', "Your new password has been set");

            return $this->redirectToRoute('index');
        }

        return $this->render('index/reset-password-confirm.html.twig', [
            'form' => $form->createView()]
        );
    }
}