<?php

namespace App\Controller;

use App\Entity\Counter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class CounterController extends AbstractController
{
    public function generateCode(): Response
    {
        $randomValue = bin2hex(random_bytes(20));

        $em = $this->getDoctrine()->getManager();

        $code = new Counter();

        $code->setCode($randomValue);

        $em->persist($code);
        $em->flush();

        $code = "<script src='//pixel.alphastream.ru/watch/{$randomValue}'></script>";

        return new Response($code);
    }
}
