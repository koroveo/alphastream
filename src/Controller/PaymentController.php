<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PaymentController extends AbstractController
{
    public function index()
    {
        return $this->render('payment/index.html.twig', [
            'controller_name' => 'PaymentController',
            'robokassaPaymentUrl' => file_get_contents('https://bill.alphastream.ru/robokassa/get_payment_url')
        ]);
    }
}
