<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Entity\DbPoolData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SyncController extends AbstractController
{
    const BILLING = 'https://bill.alphastream.ru/api/v1/withdrawal/by_identified_phone/1';

    public function receiveDataFromDbPool(Request $request): Response
    {
        if ($dbPoolReceivedDecodedData = json_decode($request->getContent(), true)) {
            // Prepare data for activities update
            $dbPoolDataForActivitiesUpdate = array_map('unserialize', array_unique(array_map('serialize', $dbPoolReceivedDecodedData))); // Remove duplicate array entries from multidimensional array

            $dbPoolTempData = [];
            foreach ($dbPoolDataForActivitiesUpdate as $dbPoolItemForActivitiesUpdate) {
                $dbPoolTempData[$dbPoolItemForActivitiesUpdate['partner_sess']][] = [
                    'email' => $dbPoolItemForActivitiesUpdate['email'],
                    'phone' => $dbPoolItemForActivitiesUpdate['phone'],
                ];
            }
            $dbPoolDataForActivitiesUpdate = $dbPoolTempData;

            $dbPoolTempData = [];
            foreach ($dbPoolDataForActivitiesUpdate as $partnerSessionId => $dbPoolItemForActivitiesUpdate) {
                foreach ($dbPoolItemForActivitiesUpdate as $dbPoolSubItemForActivitiesUpdate) {
                    $dbPoolTempData[$partnerSessionId]['email'][] = $dbPoolSubItemForActivitiesUpdate['email'];
                    $dbPoolTempData[$partnerSessionId]['phone'][] = $dbPoolSubItemForActivitiesUpdate['phone'];
                }

                $dbPoolTempData[$partnerSessionId]['email'] = array_values(array_filter(array_unique($dbPoolTempData[$partnerSessionId]['email'])));
                $dbPoolTempData[$partnerSessionId]['phone'] = array_values(array_filter(array_unique($dbPoolTempData[$partnerSessionId]['phone'])));
            }
            $dbPoolDataForActivitiesUpdate = $dbPoolTempData;

            $dbPoolTempData = [];
            foreach ($dbPoolDataForActivitiesUpdate as $partnerSessionId => $dbPoolItemForActivitiesUpdate) {
                $dbPoolTempData[$partnerSessionId]['email'] = array_slice($dbPoolItemForActivitiesUpdate['email'], 0, 3, true);
                $dbPoolTempData[$partnerSessionId]['phone'] = array_slice($dbPoolItemForActivitiesUpdate['phone'], 0, 3, true);
            }
            $dbPoolDataForActivitiesUpdate = $dbPoolTempData;

            $dbPoolDataForActivitiesUpdate = array_combine(array_keys($dbPoolDataForActivitiesUpdate), $dbPoolTempData);

            $this->updateAlphastreamData($dbPoolDataForActivitiesUpdate);

            $em = $this->getDoctrine()->getManager();

            foreach ($dbPoolReceivedDecodedData as $dbPoolReceivedDecodedItem) {
                $dbPoolData = new DbPoolData();

                $dbPoolReceivedDecodedItem['caltat_sess'] and $dbPoolData->setCaltatSId($dbPoolReceivedDecodedItem['caltat_sess']);
                $dbPoolReceivedDecodedItem['partner_sess'] and $dbPoolData->setPartnerSId($dbPoolReceivedDecodedItem['partner_sess']);
                $dbPoolReceivedDecodedItem['email'] and $dbPoolData->setEmail($dbPoolReceivedDecodedItem['email']);
                $dbPoolReceivedDecodedItem['phone'] and $dbPoolData->setPhone($dbPoolReceivedDecodedItem['phone']);
                $dbPoolReceivedDecodedItem['pid'] and $dbPoolData->setPartnerId($dbPoolReceivedDecodedItem['pid']);

                $em->persist($dbPoolData);
                $em->flush();
            }

            return new Response('success');
        } else {
            return new Response('fail', 500);
        }
    }

    private function updateAlphastreamData(array $dbPoolDataForActivitiesUpdate): void
    {
        $em = $this->getDoctrine()->getManager();

        foreach ($dbPoolDataForActivitiesUpdate as $partnerSessionId => $dbPoolReceivedPreparedDecodedItem) {
            $activities = $this
                ->getDoctrine()
                ->getRepository(Activity::class)
                ->createQueryBuilder('a')
                ->andWhere('a.cookieValue = :cookieValue')
                ->andWhere('a.dateCreated > :date')
                ->setParameter('cookieValue', $partnerSessionId)
                ->setParameter('date', (new \DateTime())->modify('-5 minutes'))
                ->getQuery()
                ->execute();

            if (isset($dbPoolReceivedPreparedDecodedItem['email'])) {
                $emails = $dbPoolReceivedPreparedDecodedItem['email'];
            }

            if (isset($dbPoolReceivedPreparedDecodedItem['phone'])) {
                $phones = $dbPoolReceivedPreparedDecodedItem['phone'];
            }

            /** @var Activity $activity */
            foreach ($activities as $activity) {
                if (isset($emails)) {
                    if (isset($emails[0])) {
                        $activity->setEmail($emails[0]);
                    }

                    if (isset($emails[1])) {
                        $activity->setEmail2($emails[1]);
                    }

                    if (isset($emails[2])) {
                        $activity->setEmail3($emails[2]);
                    }
                }

                if (isset($phones)) {
                    if (isset($phones[0])) {
                        $activity->setPhone($phones[0]);
                    }

                    if (isset($phones[1])) {
                        $activity->setPhone2($phones[1]);
                    }

                    if (isset($phones[2])) {
                        $activity->setPhone3($phones[2]);
                    }
                }

                file_get_contents(self::BILLING);

                $em->persist($activity);
                $em->flush();
            }
        }
    }
}
