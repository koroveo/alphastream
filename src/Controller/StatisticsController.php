<?php

namespace App\Controller;

use App\Entity\Activity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class StatisticsController extends AbstractController
{
    public function index()
    {
        return $this->render('statistics/index.html.twig', [
            'controller_name' => 'StatisticsController',
        ]);
    }

    public function getAjaxData()
    {
        $activities = $this
            ->getDoctrine()
            ->getRepository(Activity::class)
            ->findAll();

        $activitiesForJsonEncoding = [];
        /** @var Activity $activity */
        foreach ($activities as $activity) {
            $phones = [];

            if (!is_null($activity->getPhone())) {
                $phones[] = $activity->getPhone();
            }

            if (!is_null($activity->getPhone2())) {
                $phones[] = $activity->getPhone2();
            }

            if (!is_null($activity->getPhone3())) {
                $phones[] = $activity->getPhone3();
            }

            $emails = [];

            if (!is_null($activity->getEmail())) {
                $emails[] = $activity->getEmail();
            }

            if (!is_null($activity->getEmail2())) {
                $emails[] = $activity->getEmail2();
            }

            if (!is_null($activity->getEmail3())) {
                $emails[] = $activity->getEmail3();
            }

            $activitiesForJsonEncoding['data'][] = [
                'date' => $activity->getDateCreated()->format('d.m.Y H:i'),
                'url' => $activity->getUrl(),
                'ip' => $activity->getIp(),
                'phone' => implode(', ', $phones),
                'email' => implode(', ', $emails)
            ];
        }

        return new JsonResponse($activitiesForJsonEncoding);
    }
}
