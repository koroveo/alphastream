<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Entity\Counter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WatchController extends AbstractController
{
    const CALTAT_URL = '//cdn3.caltat.com/a76e0953-50f7-4014-b09b-4c709d21516d/spixel.php?usid=';

    public function index(string $counterCode, Request $request): Response
    {
        if ($request->cookies->get('alphastream_temp')) {
            $this->addActivity($counterCode, $request->cookies->get('alphastream'), $request);

            return new Response();
        } else {
            $tempCookieName = 'alphastream_temp';
            $tempCookieValue = $randomValue = bin2hex(random_bytes(20));
            $tempCookieExpires = time() + 86400; // 1 day
            $tempCookie = new Cookie($tempCookieName, $tempCookieValue, $tempCookieExpires);

            if ($request->cookies->get('alphastream')) {
                $permanentCookieValue = $request->cookies->get('alphastream');

                $response = new RedirectResponse(self::CALTAT_URL . $permanentCookieValue);
            } else {
                $cookieName = 'alphastream';
                $permanentCookieValue = $randomValue = bin2hex(random_bytes(20));
                $cookieExpires = time() + 315360000; // 10 years
                $permanentCookie = new Cookie($cookieName, $permanentCookieValue, $cookieExpires);

                $response = new RedirectResponse(self::CALTAT_URL . $permanentCookieValue);
                $response->headers->setCookie($permanentCookie);
            }

            $response->headers->setCookie($tempCookie);

            $this->addActivity($counterCode, $permanentCookieValue, $request);

            return $response->send();
        }
    }

    private function addActivity(string $counterCode, string $cookieValue, Request $request): void
    {
        $em = $this->getDoctrine()->getManager();
        $activity = new Activity();

        $counter = $em
            ->getRepository(Counter::class)
            ->findBy([
                'code' => $counterCode
            ]);
        $counter = $counter[0];

        $url = $request->headers->get('referer') ?: $request->getUri();

        $activity->setCounterId($counter->getId());
        $activity->setUrl($url);
        $activity->setIp($request->getClientIp());
        $activity->setCookieValue($cookieValue);

        $em->persist($activity);
        $em->flush();
    }
}
