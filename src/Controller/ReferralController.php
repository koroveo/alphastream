<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReferralController extends AbstractController
{
    public function index()
    {
        return $this->render('referral/index.html.twig', [
            'controller_name' => 'ReferralController'
        ]);
    }
}
