<?php

namespace App\EntityTrait;

trait BaseTrait
{
    /**
     * @var int Id
     */
    protected $id;

    /**
     * @var bool Is active flag
     */
    protected $isActive = true;

    /**
     * @var \DateTime Date and time of creation
     */
    protected $dateCreated;

    /**
     * @var \DateTime Update date and time
     */
    protected $dateUpdated;

    /**
     * Gets id
     *
     * @return int Id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Sets is active flag
     *
     * @param bool $isActive Is active flag
     *
     * @return self This object
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Gets is active flag
     *
     * @return bool Is active flag
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * Gets create date
     *
     * @return \DateTime Date of creation
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * Gets update date
     *
     * @return \DateTime Update date
     */
    public function getDateUpdated(): \DateTime
    {
        return $this->dateUpdated;
    }

    /**
     * Generates current date and time when creating an object
     *
     * @return self This object
     */
    public function generateDateCreated(): self
    {
        $this->dateCreated = new \DateTime();

        return $this;
    }

    /**
     * Generates current date and time when updating an object
     *
     * @return self This object
     */
    public function generateDateUpdated(): self
    {
        $this->dateUpdated = new \DateTime();

        return $this;
    }
}