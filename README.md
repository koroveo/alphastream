# Подготовка проекта к работе
- Создать виртуальный хост в nginx cо следующим конфигом:
```
server {
    server_name alphastream;
    root /var/www/alphastream/public;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        include snippets/fastcgi-php.conf;

        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;

        internal;
    }

    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/alphastream_error.log;
    access_log /var/log/nginx/alphastream_access.log;
}

```
- Выполнить `composer install` для установки пакетов composer
- Выполнить `npm install` для установки пакетов npm
- Выполнить `yarn install` для установки пакетов yarn
- Выполнить `php bin/console fos:js-routing:dump --format=json` для генерации js-роутов
- Выполнить `yarn encore dev` для генерации assets для фронтенда в папке `/public/build`
- В файле `.env` указать `user` и `password` для БД в `DATABASE_URL`
- Создать в PostgreSQL БД `alphastream_dev_username`
- Выполнить `php bin/console doctrine:migrations:migrate` для создания структуры БД
- Добавить git hook для автоматического добавления названия ветки перед названием коммита. Для этого нужно выполнить `nano .git/hooks/prepare-commit-msg` для создания файла и поместить в него содержимое:
```
#!/bin/bash

# This way you can customize which branches should be skipped when
# prepending commit message. 
if [ -z "$BRANCHES_TO_SKIP" ]; then
  BRANCHES_TO_SKIP=(master develop test)
fi

BRANCH_NAME=$(git symbolic-ref --short HEAD)
BRANCH_NAME="${BRANCH_NAME##*/}"

BRANCH_EXCLUDED=$(printf "%s\n" "${BRANCHES_TO_SKIP[@]}" | grep -c "^$BRANCH_NAME$")
BRANCH_IN_COMMIT=$(grep -c "\[$BRANCH_NAME\]" $1)

if [ -n "$BRANCH_NAME" ] && ! [[ $BRANCH_EXCLUDED -eq 1 ]] && ! [[ $BRANCH_IN_COMMIT -ge 1 ]]; then 
  sed -i.bak -e "1s/^/[$BRANCH_NAME] /" $1
fi
```

# Создание entity
- Создать `EntityName.yml` в `src/Resources/config/doctrine`. Прописать в файле маппинг для `EntityName`
- Создать entity `EntityName.php` в `src/Entity`. Прописать в классе маппинг для `EntityName`
- Создать репозиторий `EntityNameRepository.php` для этого entity в `src/Repository`
- Выполнить `php bin/console doctrine:migrations:diff` для генерации миграции
- Выполнить `php bin/console doctrine:migrations:migrate` для применения миграции к БД

# Изменение entity
- Изменить `EntityName.yml` в `src/Resources/config/doctrine`
- Изменить `EntityName.php` в `src/Entity`
- Выполнить `php bin/console doctrine:migrations:diff` для генерации миграции
- Выполнить `php bin/console doctrine:migrations:migrate` для применения миграции к БД

# Полезные плагины для PhpStorm
Эти плагины делают работу с PHP и Symfony более удобной: добавляют дополнительный автокомплит, подсветку кода, и т.д.

- Symfony Plugin
- PHP Toolbox